

/*
			Ejercicio.
			
			Crear un programa en SCALA como se indica a continuaci�n:
			
			1. Llenar un archivo de forma aleatoria con lineas de 1' y 0' que identifican las imagenes de matr�culas
			   tomadas por una camara y almacenadas en dicho archivo.
			   
			   * un mill�n de 1' y 0'
			   * Un mill�n de matr�culas
			
			2. Leer el archivo y buscar en cada l�nea patrones de 1001 juntos.
			
			3. Implementar funciones CBV y CBN para medir los tiempos de an�lisis.
			
			Utilizar evaluaci�n perezosa para analizar los datos solo cuando sea necesario.
			
*/


import java.io._
import scala.util.Random


object Prueba {
  
  def ingresarNumeros(): Unit = {
    
    val file = new File("matriculas.txt")
    val escribir = new PrintWriter(file)
    
    for(x <- 0 to 1000) {
      var num: Int = generarNumAleatorio()
      
      for(y <- 0 to 1000) {
        var num: Int = generarNumAleatorio()
        
        escribir.write(num)
      }
      escribir.write("\n")
    }
    escribir.close()
  }// Ingresar numeros
  
  def generarNumAleatorio(): Int = {
    
    var num = (Math.random() * 5 + 1).toInt
    var n: Int = 0
    
    if(num <= 5) {
      if(num == 1) {
        n = 0
      
      } else {
        if(num == 2) {
          n = 1
        
        } else {
          if(num == 3) {
            n = 0
          
          } else {
            if(num == 4) {
              n = 1
            
            } else {
              if(num == 5) {
                n = 0
              
              } else {
                n = 1
              }
            }
          }
        }
      }
    }
    
    n
  }// Generar n�meros aleatorios
  
  def main(args: Array[String]): Unit = {
    
    println("\n *=*=*=*=*=*=*=*=*=*=* LAZY EVALUATION *=*=*=*=*=*=*=*=*=*=*")
    
    ingresarNumeros()
  }
}
